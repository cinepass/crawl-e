"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var RequestMaker_1 = require("./RequestMaker");
var path = require("path");
var fs = require("fs");
var nock = require("nock");
/**
 * A request make that add caching via the [nock](https://github.com/nock/nock) module to speed up the crawling execution time during development.
 *
 * @category RequestMaker
 */
var RequestMakerWithNockCaching = /** @class */ (function (_super) {
    __extends(RequestMakerWithNockCaching, _super);
    function RequestMakerWithNockCaching(config, cacheDir) {
        var _this = _super.call(this, config) || this;
        _this.cacheDir = cacheDir;
        _this.nockFile = null;
        return _this;
    }
    RequestMakerWithNockCaching.prototype.willStartCrawling = function () {
        this.nockFile = path.resolve() + '/' + this.cacheDir + '/' + this.nockFilename();
        if (fs.existsSync(this.nockFile)) {
            this.logger.info('Using cache requests from ', this.nockFile);
            nock.load(this.nockFile);
        }
        else {
            console.info('Start recording requests');
            nock.recorder.rec({
                dont_print: true,
                output_objects: true
            });
        }
    };
    RequestMakerWithNockCaching.prototype.didFinishCrawling = function () {
        if (fs.existsSync(this.nockFile)) {
            return;
        }
        var nockObjects = nock.recorder.play();
        try {
            fs.mkdirSync(this.cacheDir);
        }
        catch (e) {
            if (e.code !== 'EEXIST') {
                throw e;
            }
        }
        fs.writeFileSync(this.nockFile, JSON.stringify(nockObjects, null, 2));
        this.logger.info('Saved recorded request: ' + this.nockFile);
    };
    RequestMakerWithNockCaching.prototype.nockFilename = function () {
        var crawlEConfig = this.config;
        return crawlEConfig.crawler.id + '.nock.json';
    };
    return RequestMakerWithNockCaching;
}(RequestMaker_1.DefaultRequestMaker));
exports.default = RequestMakerWithNockCaching;
