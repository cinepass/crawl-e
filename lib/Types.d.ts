export declare type Callback<T> = (err: Error, result?: T) => void;
export declare type ListParsingCallback<T> = (err: Error, result?: T[], nextPageUrl?: string) => void;
