export type Callback<T> = (err: Error, result?: T) => void
export type ListParsingCallback<T> = (err: Error, result?: T[], nextPageUrl?: string) => void