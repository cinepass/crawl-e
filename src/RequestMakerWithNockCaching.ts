import { DefaultRequestMaker } from './RequestMaker';
import Config from './Config';
import * as path from 'path'
import * as fs from 'fs'
import * as nock from 'nock'

/**
 * A request make that add caching via the [nock](https://github.com/nock/nock) module to speed up the crawling execution time during development. 
 * 
 * @category RequestMaker
 */
class RequestMakerWithNockCaching extends DefaultRequestMaker {
  nockFile: any = null

  constructor(config: Config, private cacheDir: string) {
    super(config)
  }

  willStartCrawling() {
    this.nockFile = path.resolve() + '/' + this.cacheDir + '/' + this.nockFilename()
    if (fs.existsSync(this.nockFile)) {
      this.logger.info('Using cache requests from ', this.nockFile)
      nock.load(this.nockFile)
    } else {
      console.info('Start recording requests')
      nock.recorder.rec({
        dont_print: true,
        output_objects: true
      })
    }
  }

  didFinishCrawling() {
    if (fs.existsSync(this.nockFile)) {
      return
    }

    var nockObjects = nock.recorder.play()
    try {
      fs.mkdirSync(this.cacheDir)
    } catch (e) {
      if (e.code !== 'EEXIST') { throw e }
    }
    fs.writeFileSync(this.nockFile, JSON.stringify(nockObjects, null, 2))
    this.logger.info('Saved recorded request: ' + this.nockFile)
  }

  private nockFilename() {
    let crawlEConfig = this.config as Config 
    return crawlEConfig.crawler.id + '.nock.json'
  }
}

export default RequestMakerWithNockCaching
