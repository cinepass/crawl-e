
import CrawlE from './CrawlE'
export * from './Context'
import Utils from './Utils'
import { DatesParsing } from './parsers/DatesParsing'
(Utils as any).parseDates = DatesParsing.parseDates


export * from './JsonFileWriter'

import { DefaultLogger } from './Logger'
import { BaseHtmlParser } from './ResponseParsers'
import ValueGrabber from './ValueGrabber'
import { DefaultRequestMaker, CachedRequestMaker } from './RequestMaker'
import { DefaultContext } from './Context'
import { JsonFileWriter } from './JsonFileWriter'


export * from './parsers'

export {
  BaseHtmlParser,
  CachedRequestMaker,
  DefaultLogger,
  DefaultRequestMaker,
  Utils,
  ValueGrabber
}

[
  BaseHtmlParser, 
  DefaultLogger, 
  DefaultContext, 
  DefaultRequestMaker,
  CachedRequestMaker, 
  JsonFileWriter,
  ValueGrabber
].forEach(e => CrawlE[e.name] = e)

//@ts-ignore 
CrawlE.Utils = Utils

module.exports = CrawlE


